package ab.javac.context;

import ab.javac.terms.values.Value;

@SuppressWarnings("serial")
public class TypeException extends Exception {
	public TypeException(Object obj, Value expected, Value actual) {
		super("Expected " + obj + " : " + expected.quote() + ", got " + obj + " : " + actual.quote() + ".");
	}
	
	public TypeException(Object obj, String expected, Value actual) {
		super("Expected " + obj + " : " + expected + ", got " + obj + " : " + actual.quote() + ".");
	}
	
	public TypeException(Object obj, Value expected, String actual) {
		super("Expected " + obj + " : " + expected.quote() + ", got " + obj + " : " + actual + ".");
	}
}
