package ab.javac.context;

import java.util.Optional;

import ab.javac.names.Name;
import ab.javac.terms.values.Value;
import fj.Equal;
import fj.P2;
import fj.data.Java8;
import fj.data.List;

public class Context {
	private final List<Name> names;
	private final List<Value> info;
	
	private Context() {
		names = List.nil();
		info = List.nil();
	}
	
	private Context(List<Name> names, List<Value> info) {
		this.names = names;
		this.info = info;
	}
	
	public Context cons(Name name, Value info) {
		if(name == null)
			throw new IllegalArgumentException("Cannot make a type judgement of " + info + " on null.");
		if(info == null)
			throw new IllegalArgumentException("Cannot make a null type judgement on " + name);
		return new Context(names.cons(name), this.info.cons(info));
	}
	
	public Optional<Value> lookup(Name name) {
		return Java8.Option_Optional(names.elementIndex(Equal.anyEqual(), name)).map(info::index);
	}
	
	public Value get(Name name) throws UnknownIdentifierException {
		Optional<Value> info = lookup(name);
		if(info.isPresent())
			return info.get();
		throw new UnknownIdentifierException(name, this);
	}
	
	public static Context nil() {
		return new Context();
	}
	
	@Override
	public String toString() {
		// Tbh iterative is easier than a fold and stuff right now
		StringBuilder str = new StringBuilder();
		str.append('[');
		for(P2<Name, Value> judgement : names.zip(info)) {
			str.append(judgement._1() + " : " + judgement._2() + ", ");
		}
		// I'm going to hell for this
		// I just don't feel like thinking this all through
		str.deleteCharAt(str.length() - 1);
		str.deleteCharAt(str.length() - 1);
		str.append(']');
		return str.toString();
	}
}
