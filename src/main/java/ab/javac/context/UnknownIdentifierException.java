package ab.javac.context;

import ab.javac.names.Name;

@SuppressWarnings("serial")
public class UnknownIdentifierException extends Exception {
	public UnknownIdentifierException(Name name) {
		super("Unknown identifier " + name);
	}
	
	public UnknownIdentifierException(Name name, Context ctx) {
		super("Unknown identifier " + name + " in context " + ctx);
	}
}
