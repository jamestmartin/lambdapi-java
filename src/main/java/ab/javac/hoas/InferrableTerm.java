package ab.javac.hoas;

import ab.javac.context.Context;
import ab.javac.context.TypeException;
import ab.javac.context.UnknownIdentifierException;
import ab.javac.terms.values.Value;

public interface InferrableTerm extends CheckableTerm {
	@Override
	public default Value assertType(Context ctx, int index, Value expected) throws TypeException, UnknownIdentifierException {
		Value type = assertType(ctx, index);
		if(!type.quote().equals(expected.quote()))
			throw new TypeException(this, expected, type);
		return type;
	}
	
	public default CheckableTerm apply(CheckableTerm x) {
		return new Application(this, x);
	}
	
	public Value assertType(Context ctx, int index) throws TypeException, UnknownIdentifierException;
	
	public default Value assertType(Context ctx) throws TypeException, UnknownIdentifierException {
		return assertType(ctx, 0);
	}
	
	@Override
	public InferrableTerm substitute(int index, InferrableTerm value);
}
