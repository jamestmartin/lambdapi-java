package ab.javac.hoas;

import ab.javac.context.Context;
import ab.javac.context.TypeException;
import ab.javac.context.UnknownIdentifierException;
import ab.javac.names.Name;
import ab.javac.terms.values.EntirelyNeutral;
import ab.javac.terms.values.Value;
import fj.data.List;

public class Free implements InferrableTerm, EntirelyNeutral {
	private final Name name;
	
	public Free(Name name) {
		this.name = name;
	}

	@Override
	public EntirelyNeutral eval(List<Value> env) {
		return this;
	}

	@Override
	public Value assertType(Context ctx, int index) throws TypeException, UnknownIdentifierException {
		return ctx.get(name);
	}

	@Override
	public InferrableTerm substitute(int index, InferrableTerm value) {
		return this;
	}

	@Override
	public InferrableTerm quote(int index) {
		return name.quote(index);
	}
	
	@Override
	public String toString() {
		return name.toString();
	}
}
