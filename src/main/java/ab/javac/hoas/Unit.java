package ab.javac.hoas;

import ab.javac.context.Context;
import ab.javac.context.TypeException;
import ab.javac.context.UnknownIdentifierException;
import ab.javac.terms.values.EntirelyNeutral;
import ab.javac.terms.values.Value;
import fj.data.List;

public class Unit implements InferrableTerm, EntirelyNeutral {
	public static final Unit UNIT = new Unit();
	
	private Unit() { }
	
	@Override
	public EntirelyNeutral eval(List<Value> env) {
		return this;
	}

	@Override
	public EntirelyNeutral assertType(Context ctx, int index) throws TypeException, UnknownIdentifierException {
		return UNIT_TYPE;
	}

	@Override
	public InferrableTerm substitute(int index, InferrableTerm value) {
		return this;
	}

	@Override
	public InferrableTerm quote(int index) {
		return this;
	}
	
	@Override
	public String toString() {
		return "()";
	}
}
