package ab.javac.hoas;

import ab.javac.context.Context;
import ab.javac.context.TypeException;
import ab.javac.context.UnknownIdentifierException;
import ab.javac.terms.values.EntirelyNeutral;
import ab.javac.terms.values.Value;
import fj.data.List;

public class UnitType implements InferrableTerm, EntirelyNeutral {
	public static final UnitType UNIT_TYPE = new UnitType();
	
	private UnitType() { }
	
	@Override
	public EntirelyNeutral assertType(Context ctx, int index) throws TypeException, UnknownIdentifierException {
		return CheckableTerm.KIND;
	}

	@Override
	public EntirelyNeutral eval(List<Value> env) {
		return this;
	}

	@Override
	public InferrableTerm substitute(int index, InferrableTerm value) {
		return CheckableTerm.KIND;
	}

	@Override
	public InferrableTerm quote(int index) {
		return this;
	}
	
	@Override
	public String toString() {
		return "()";
	}
}
