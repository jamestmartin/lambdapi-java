package ab.javac.hoas;

import ab.javac.context.Context;
import ab.javac.context.TypeException;
import ab.javac.context.UnknownIdentifierException;
import ab.javac.hoas.values.Neutral;
import ab.javac.terms.values.SigmaValue;
import ab.javac.terms.values.Value;
import ab.javac.terms.values.ValuePi;
import fj.data.List;

public class Sigma implements InferrableTerm {
	private final CheckableTerm a;
	private final InferrableTerm f;
	
	public static String printSigma(CheckableTerm a, CheckableTerm f) {
		return "Σ(x : " + a + "), " + f + "(x)";
	}
	
	public Sigma(CheckableTerm a, InferrableTerm f) {
		if(a == null || f == null)
			throw new IllegalArgumentException("Invalid sigma type: " + printSigma(a, f));
		this.a = a;
		this.f = f;
	}

	@Override
	public Value assertType(Context ctx, int index) throws TypeException, UnknownIdentifierException {
		Value ft = f.assertType(ctx, index);
		if(!(f instanceof ValuePi))
			throw new TypeException(f, "some pi type", ft);
		ValuePi ftp = (ValuePi) ft;
		a.assertType(ctx, index, ftp.arg());
		return KIND;
	}

	@Override
	public Neutral eval(List<Value> env) {
		Value av = a.eval(env);
		return new SigmaValue(av, f.eval(env));
	}

	@Override
	public InferrableTerm substitute(int index, InferrableTerm value) {
		return new Sigma(a.substitute(index, value), f.substitute(index, value));
	}

	@Override
	public String toString() {
		return printSigma(a, f);
	}
}
