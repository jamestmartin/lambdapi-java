package ab.javac.hoas;

import ab.javac.context.Context;
import ab.javac.context.TypeException;
import ab.javac.context.UnknownIdentifierException;
import ab.javac.names.LocalName;
import ab.javac.terms.values.Value;
import ab.javac.terms.values.ValueLambda;
import ab.javac.terms.values.ValuePi;
import fj.data.List;

public class Lambda implements CheckableTerm {
	private final CheckableTerm body;
	
	public Lambda(CheckableTerm body) {
		if(body == null)
			throw new IllegalArgumentException("Lambda body cannot be null.");
		this.body = body;
	}

	@Override
	public Value assertType(Context ctx, int index, Value expected) throws TypeException, UnknownIdentifierException {
		if(!(expected instanceof ValuePi))
			throw new TypeException(this, expected, "some pi type");
		ValuePi ftype = (ValuePi) expected;
		LocalName varName = new LocalName(index);
		body.substitute(0, varName.toFree())
			.assertType(ctx.cons(varName, ftype.arg()), index + 1, ftype.body().apply(varName.toFree()));
		return ftype;
	}

	@Override
	public Value eval(List<Value> env) {
		return new ValueLambda(x -> body.eval(env.cons(x)));
	}

	@Override
	public CheckableTerm substitute(int index, InferrableTerm value) {
		return new Lambda(body.substitute(index + 1, value));
	}
	
	@Override
	public String toString() {
		return "(λ" + body + ")";
	}
}
