package ab.javac.hoas;

import ab.javac.context.Context;
import ab.javac.context.TypeException;
import ab.javac.context.UnknownIdentifierException;
import ab.javac.terms.values.Value;
import fj.data.List;

public interface CheckableTerm {
	public static final Unit UNIT = Unit.UNIT;
	public static final UnitType UNIT_TYPE = UnitType.UNIT_TYPE;
	public static final Kind KIND = Kind.KIND;
	
	public Value assertType(Context ctx, int index, Value expected) throws TypeException, UnknownIdentifierException;
	
	public default InferrableTerm annotate(CheckableTerm t) {
		return new Annotated(this, t);
	}
	
	public Value eval(List<Value> env);
	
	public default Value eval() {
		return eval(List.nil());
	}
	
	public CheckableTerm substitute(int index, InferrableTerm value);
}
