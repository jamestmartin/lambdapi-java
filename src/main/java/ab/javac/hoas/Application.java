package ab.javac.hoas;

import ab.javac.context.Context;
import ab.javac.context.TypeException;
import ab.javac.context.UnknownIdentifierException;
import ab.javac.terms.values.Value;
import ab.javac.terms.values.ValuePi;
import fj.data.List;

public class Application implements InferrableTerm {
	private final InferrableTerm f;
	private final CheckableTerm x;
	
	public Application(InferrableTerm f, CheckableTerm x) {
		if(x == null)
			throw new IllegalArgumentException("Cannot apply " + f + " to a null argument.");
		if(f == null)
			throw new IllegalArgumentException("Cannot apply a null function to " + x);
		this.f = f;
		this.x = x;
	}

	@Override
	public Value assertType(Context ctx, int index) throws TypeException, UnknownIdentifierException {
		Value t = f.assertType(ctx, index);
		if(!(t instanceof ValuePi))
			throw new TypeException(this, "some pi type", t);
		ValuePi tp = (ValuePi) t;
		x.assertType(ctx, index, tp.arg());
		return tp.body().apply(x.eval(List.nil()));
	}

	@Override
	public Value eval(List<Value> env) {
		return f.eval(env).apply(x.eval(env));
	}

	@Override
	public InferrableTerm substitute(int index, InferrableTerm value) {
		return new Application(f.substitute(index, value), x.substitute(index, value));
	}
	
	@Override
	public boolean equals(Object obj) {
		return obj instanceof Application
				&& ((Application) obj).f.equals(this.f)
				&& ((Application) obj).x.equals(this.x);
	}
	
	@Override
	public String toString() {
		return "(" + f + " " + x + ")";
	}
}
