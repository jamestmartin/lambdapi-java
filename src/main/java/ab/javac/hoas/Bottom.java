package ab.javac.hoas;

import ab.javac.context.Context;
import ab.javac.context.TypeException;
import ab.javac.context.UnknownIdentifierException;
import ab.javac.terms.values.EntirelyNeutral;
import ab.javac.terms.values.Value;
import fj.data.List;

public class Bottom implements CheckableTerm, EntirelyNeutral {
	@Override
	public Value eval(List<Value> env) {
		return this;
	}

	@Override
	public CheckableTerm substitute(int index, InferrableTerm value) {
		return this;
	}

	@Override
	public CheckableTerm quote(int index) {
		return this;
	}

	@Override
	public Value assertType(Context ctx, int index, Value expected) throws TypeException, UnknownIdentifierException {
		return expected;
	}
}
