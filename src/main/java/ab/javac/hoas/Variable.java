package ab.javac.hoas;

import ab.javac.context.Context;
import ab.javac.context.TypeException;
import ab.javac.context.UnknownIdentifierException;
import ab.javac.terms.values.Value;
import fj.data.List;

public class Variable implements InferrableTerm {
	private final int index;
	
	public Variable(int index) {
		this.index = index;
	}

	@Override
	public Value eval(List<Value> env) {
		return env.index(index);
	}

	@Override
	public Value assertType(Context ctx, int index) throws TypeException, UnknownIdentifierException {
		throw new IllegalStateException("A bound variable " + index + " missed substitution in typechecking!");
	}

	@Override
	public InferrableTerm substitute(int index, InferrableTerm value) {
		if(index == this.index)
			return value;
		return this;
	}
	
	@Override
	public String toString() {
		return Integer.toString(index);
	}
}
