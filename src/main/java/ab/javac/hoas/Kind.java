package ab.javac.hoas;

import ab.javac.context.Context;
import ab.javac.context.TypeException;
import ab.javac.context.UnknownIdentifierException;
import ab.javac.terms.values.EntirelyNeutral;
import ab.javac.terms.values.Value;
import fj.data.List;

public class Kind implements InferrableTerm, EntirelyNeutral {
	public static final Kind KIND = new Kind(0);
	
	private final int rank;
	
	public Kind(int rank) {
		this.rank = rank;
	}
	
	@Override
	public EntirelyNeutral eval(List<Value> env) {
		return this;
	}

	@Override
	public EntirelyNeutral assertType(Context ctx, int index) throws TypeException, UnknownIdentifierException {
		return new Kind(rank + 1);
	}

	@Override
	public InferrableTerm substitute(int index, InferrableTerm value) {
		return this;
	}

	@Override
	public InferrableTerm quote(int index) {
		return this;
	}
	
	@Override
	public String toString() {
		return "*" + rank;
	}
}
