package ab.javac.hoas;

import ab.javac.context.Context;
import ab.javac.context.TypeException;
import ab.javac.context.UnknownIdentifierException;
import ab.javac.terms.values.Value;
import fj.data.List;

public class Annotated implements InferrableTerm {
	public final CheckableTerm e;
	public final CheckableTerm t;
	
	public Annotated(CheckableTerm e, CheckableTerm t) {
		this.e = e;
		this.t = t;
	}

	@Override
	public Value eval(List<Value> env) {
		return e.eval(env);
	}

	@Override
	public InferrableTerm substitute(int index, InferrableTerm value) {
		return new Annotated(e.substitute(index, value), t);
	}

	@Override
	public Value assertType(Context ctx, int index) throws TypeException, UnknownIdentifierException {
		t.assertType(ctx, index, KIND);
		Value v = t.eval(List.nil());
		e.assertType(ctx, index, v);
		return v;
	}
	
	@Override
	public String toString() {
		return e + " : " + t;
	}
}
