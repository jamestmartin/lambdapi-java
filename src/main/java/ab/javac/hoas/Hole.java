package ab.javac.hoas;

import ab.javac.context.Context;
import ab.javac.context.TypeException;
import ab.javac.context.UnknownIdentifierException;
import ab.javac.terms.values.Value;
import fj.data.List;

public class Hole implements CheckableTerm {
	private final String name;
	
	public Hole(String name) {
		this.name = name;
	}

	@Override
	public Value assertType(Context ctx, int index, Value expected) throws TypeException, UnknownIdentifierException {
		throw new TypeException(this, expected, "hole");
	}

	@Override
	public Value eval(List<Value> env) {
		throw new UnsupportedOperationException("Cannot evaluate a hole.");
	}

	@Override
	public CheckableTerm substitute(int index, InferrableTerm value) {
		return this;
	}
	
	@Override
	public String toString() {
		return "_" + name;
	}
}
