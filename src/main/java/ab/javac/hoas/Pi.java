package ab.javac.hoas;

import ab.javac.context.Context;
import ab.javac.context.TypeException;
import ab.javac.context.UnknownIdentifierException;
import ab.javac.names.LocalName;
import ab.javac.terms.values.Value;
import ab.javac.terms.values.ValuePi;
import fj.data.List;

public class Pi implements InferrableTerm {
	private final CheckableTerm arg;
	private final CheckableTerm body;
	
	public Pi(CheckableTerm arg, CheckableTerm body) {
		if(arg == null)
			throw new IllegalArgumentException("The argument of a dependent type cannot be null.");
		if(body == null)
			throw new IllegalArgumentException("The body of a dependent type with argument " + arg + " cannot be null.");
		this.arg = arg;
		this.body = body;
	}
	
	public CheckableTerm arg() {
		return arg;
	}
	
	public CheckableTerm body() {
		return body;
	}

	@Override
	public Value eval(List<Value> env) {
		return new ValuePi(arg.eval(env), x -> body.eval(env.cons(x)));
	}

	@Override
	public Value assertType(Context ctx, int index) throws TypeException, UnknownIdentifierException {
		arg.assertType(ctx, index, KIND);
		Value v = arg.eval(List.nil());
		body.substitute(0, new LocalName(index).toFree()).assertType(ctx.cons(new LocalName(index), v), index + 1, KIND);
		return KIND;
	}

	@Override
	public InferrableTerm substitute(int index, InferrableTerm value) {
		return new Pi(arg.substitute(index, value), body.substitute(index + 1, value));
	}
	
	@Override
	public String toString() {
		return "∀x : " + arg() + "." + body();
	}
}
