package ab.javac.names;

import ab.javac.hoas.InferrableTerm;
import ab.javac.hoas.Variable;

public class QuoteName implements Name {
	private final int index;
	
	public QuoteName(int index) {
		this.index = index;
	}
	
	@Override
	public String toString() {
		return "U" + index;
	}

	@Override
	public InferrableTerm quote(int index) {
		return new Variable(index - this.index - 1);
	}
}
