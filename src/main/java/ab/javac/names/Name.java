package ab.javac.names;

import ab.javac.hoas.Free;
import ab.javac.hoas.InferrableTerm;

public interface Name {
	public default Free toFree() {
		return new Free(this);
	}
	
	public default InferrableTerm quote(int index) {
		return this.toFree();
	}
}
