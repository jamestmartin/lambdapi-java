package ab.javac.names;

public class GlobalName implements Name {
	private final String name;
	
	public GlobalName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return name;
	}
}
