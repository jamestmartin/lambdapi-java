package ab.javac.names;

import ab.javac.hoas.InferrableTerm;
import ab.javac.hoas.Variable;

public class LocalName implements Name {
	private final int index;
	
	public LocalName(int index) {
		this.index = index;
	}
	
	@Override
	public String toString() {
		return Integer.toString(index);
	}
	
	@Override
	public InferrableTerm quote(int index) {
		return new Variable(this.index + index - 1);
	}
}
