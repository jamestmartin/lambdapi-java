package ab.javac.terms.values;

import ab.javac.hoas.Application;
import ab.javac.hoas.InferrableTerm;

public class NeutralApplication implements ApplicationNeutral {
	private final ApplicationNeutral f;
	private final Value x;
	
	public NeutralApplication(ApplicationNeutral applicationNeutral, Value x) {
		if(x == null)
			throw new IllegalArgumentException("The function " + applicationNeutral + " cannot be applied to a null value");
		if(applicationNeutral == null)
			throw new IllegalArgumentException("A null function cannot be applied to the value " + x);
		this.f = applicationNeutral;
		this.x = x;
	}

	@Override
	public InferrableTerm quote(int index) {
		return new Application(f.quote(index), x.quote(index));
	}
	
	@Override
	public String toString() {
		return this.quote().toString();
	}
}
