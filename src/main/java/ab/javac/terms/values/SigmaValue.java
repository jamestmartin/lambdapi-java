package ab.javac.terms.values;

import ab.javac.hoas.InferrableTerm;
import ab.javac.hoas.Sigma;

public class SigmaValue implements EntirelyNeutral {
	private final Value a;
	private final Value f;
	
	public SigmaValue(Value a, Value f) {
		if(a == null || f == null)
			throw new IllegalArgumentException("Illegal sigma args " + a + " and " + f);
		this.a = a;
		this.f = f;
	}

	@Override
	public Value apply(Value arg) {
		return null;
	}

	@Override
	public InferrableTerm quote(int index) {
		return new Sigma(a.quote(index), f.quote(index));
	}
}