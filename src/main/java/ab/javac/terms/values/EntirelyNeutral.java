package ab.javac.terms.values;

public interface EntirelyNeutral extends ApplicationNeutral, ProductElimNeutral {
	@Override
	default Value apply(Value arg) {
		return ApplicationNeutral.super.apply(arg);
	}
}
