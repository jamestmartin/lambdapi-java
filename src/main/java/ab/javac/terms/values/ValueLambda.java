package ab.javac.terms.values;

import java.util.function.Function;

import ab.javac.hoas.Lambda;
import ab.javac.hoas.CheckableTerm;
import ab.javac.names.QuoteName;

public class ValueLambda implements Value {
	private Function<Value, Value> body;
	
	public ValueLambda(Function<Value, Value> body) {
		if(body == null)
			throw new IllegalArgumentException("A lambda's body cannot be null.");
		this.body = body;
	}

	@Override
	public Value apply(Value arg) {
		return body.apply(arg);
	}

	@Override
	public CheckableTerm quote(int index) {
		return new Lambda(body.apply(new QuoteName(index).toFree()).quote(index + 1));
	}
	
	@Override
	public String toString() {
		return this.quote().toString();
	}
}
