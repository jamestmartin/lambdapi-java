package ab.javac.terms.values;

public interface ApplicationNeutral extends Value {
	@Override
	public default Value apply(Value arg) {
		return new NeutralApplication(this, arg);
	}
}
