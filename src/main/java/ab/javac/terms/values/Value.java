package ab.javac.terms.values;

import ab.javac.hoas.InferrableTerm;

public interface Value {
	public Value apply(Value arg);
	
	public InferrableTerm quote(int index);
	
	public default InferrableTerm quote() {
		return quote(0);
	}
}
