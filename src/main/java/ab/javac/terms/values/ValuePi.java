package ab.javac.terms.values;

import java.util.function.Function;

import ab.javac.hoas.Pi;
import ab.javac.hoas.CheckableTerm;
import ab.javac.names.QuoteName;

public class ValuePi implements Value {
	protected final Value arg;
	protected final Function<Value, Value> body;
	
	public ValuePi(Value arg, Function<Value, Value> body) {
		if(arg == null)
			throw new IllegalArgumentException("The argument of a Pi value cannot be null.");
		if(body == null)
			throw new IllegalArgumentException("The body of a Pi value with argument " + arg + " cannot be null.");
		this.arg = arg;
		this.body = body;
	}
	
	public Value arg() {
		return arg;
	}
	
	public Function<Value, Value> body() {
		return body;
	}

	@Override
	public Value apply(Value arg) {
		return body.apply(arg);
	}

	@Override
	public InferrableTerm quote(int index) {
		return new Pi(arg.quote(index), body.apply(new QuoteName(index).toFree()).quote(index + 1));
	}
	
	@Override
	public String toString() {
		return this.quote().toString();
	}
}
