package ab.javac;

import ab.javac.names.LocalName;
import ab.javac.hoas.Application;
import ab.javac.hoas.Lambda;
import ab.javac.hoas.Pi;
import ab.javac.hoas.CheckableTerm;
import ab.javac.names.GlobalName;

public class Main {
	public static void main(String[] args) {
		System.out.print(
			new Application(
					new Lambda(new LocalName(1).quote(0))
						.annotate(new Pi(CheckableTerm.KIND, new LocalName(0).toFree())),
					new GlobalName("x").toFree()).eval());
	}
}
